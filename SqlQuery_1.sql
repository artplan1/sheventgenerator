﻿SELECT SYSTEM.TIMESTAMP as TimeValue, input.DEVICEID, input.Type, sl.Name as SensorName, AVG(input.MainValue) as MainValue 
FROM input INNER JOIN SensorList sl on sl.SensorId = input.DeviceId
GROUP BY input.DeviceId, input.Type, sl.Name, TUMBLINGWINDOW(minute, 1)