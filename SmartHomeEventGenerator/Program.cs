﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeEventGenerator
{
    class Program
    {
        static void Main()
        {
            send();
        }

        static async void send()
        {
            var eventHubClient = EventHubClient.Create("smarthome-eventhub");

            Random rnd = new Random();
            Parallel.For(1, 100, async (id) =>
                {
                    int count = 1;
                    int mbase = rnd.Next(20, 50);
                    while (true)
                    {
                        int p = 5;

                        if ((rnd.Next()%2 == 0) && (rnd.Next()%2 == 0))
                        {
                            p = 20;
                        }

                        Console.WriteLine("Sending event nr {0} for device {1}", count, id);
                        var reading = new SmartHomeEvent
                        {
                            sensorId = id,
                            mainValue = mbase + p * rnd.NextDouble(),
                            timeValue = DateTime.Now
                        };
                        EventData msg = new EventData(Encoding.UTF8.GetBytes(reading.ToJson()));
                        await eventHubClient.SendAsync(msg);
                        count++;
                    }
                }
            );
 
            Console.ReadKey();
        }
    }
}
