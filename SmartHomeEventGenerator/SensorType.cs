﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeEventGenerator
{
    public enum MeasureType
    {
        Temperature = 0,
        SwitchStatus = 1,
        LightStatus = 2,
        CurtainStatus = 3,
        Weight = 4,
        Distance = 5,
        Daylight = 6,
        WaterLevel = 7
    }
}
