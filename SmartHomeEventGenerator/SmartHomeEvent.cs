﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeEventGenerator
{
    class SmartHomeEvent
    {
        public int sensorId { get; set; }
        public DateTime timeValue { get; set; }
        public object mainValue { get; set; }

        public string ToJson()
        {
            dynamic eventMessage = new ExpandoObject();
            eventMessage.sensorId = sensorId;
            eventMessage.mainValue = mainValue;
            eventMessage.timeValue = timeValue;
            return JsonConvert.SerializeObject(eventMessage);
        }
    }
}
